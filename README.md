# Modding Skyrim SE on Linux with Lutris and Proton (with SkyUI working)

## Contents

1. Preparation

2. Sound Issues

3. SKSE

4. Lutris, Vortex and Modding



## 1. Preparation

This tutorial is composed of several tools and things I came about when first starting to mod skyrim special edition on linux. We will use several tools and tricks to make this work.
So first of all we need a copy of skyrim special edition (obviously), go to the steam store and buy one, if you have not already. It is very important, that we have a clean install of skyrim SE, 
if not go ahead and reinstall it (you will probably have to delete your data folder inside of the Skyrim SE folder located at ```/<path to SteamLibrary>/steamapps/common/Skyrim Special Edition/```).

Also make sure, that you have fullfilled all requirements to be able to use Proton on Steam (see [here](https://github.com/ValveSoftware/Proton/wiki/Requirements)).

When this is done, we are finally ready to get started. Probably there are other methods, maybe also simpler ones but this is the way I did it and it worked very well for me.

For completeness, your save games go here
```
/<path to SteamLibrary>/steamapps/compatdata/489830/pfx/drive_c/users/steamuser/My Documents/My Games/
```
and your Data folder is located here
```
/<path to SteamLibrary>/steamapps/common/Skyrim Special Edition/
```

## 2. Sound Issues

If you would try to start the game now, you would notice, that there is now sound or music in your Skyrim SE installation. There is a nice fix for this provided [here](https://github.com/spooknik/SkyrimSE-Linux-Modding)
which is already a quite nice tutorial about the topic. We will only use the sound fix provided there, which works as follows.

First download the latest version of [SkyrimSE audio fix](https://github.com/spooknik/SkyrimSE-Linux-Modding/releases/tag/1.0.2), which at this time is skyrim-se-audio-fix-1.0.2.zip.
You can unzip this anywhere on your computer, but keep in mind, that after the installation it can not be moved to a different location. Personally I installed it in my Documents folder.

Now we need to make a slight adjustment to the install_audio.sh file, it is very possible, that the ```WINEPREFIX``` path inside the ```install_audio.sh``` file does not correspond with your installation, the path should be something like this:

```
WINEPREFIX="/<path to your SteamLibrary>/steamapps/compatdata/489830/pfx/" ./wine_setup_faudio.sh
```
where you need to change ```<path to your SteamLibrary>``` to your according path. When this is done save the file.

Next open a terminal and execute the following (it is important that the terminal is opened with a path leading to the directory where install_audio.sh is)

```
./install_audio.sh
```
Now just wait for the process to finish, if there is an error either your path specified above was wrong or the file is not executable (if the file is not executable type the following in your terminal ```sudo chmod +x install_audio.sh```, then try the above command again).

Congratulations, your game should now work with sound!

If you do not weant to use mods with your Skyrim SE you can just start playing as it is, but for the rest, continue to the next step.


## 3. SKSE

The next step will involve a bit more work and tweaking, but do not worry nothing is impossible!
First of all we need to install a custom version of Proton, which is provided [here](https://github.com/GloriousEggroll/proton-ge-custom/releases), when I installed it I used Proton-5.0-GE-1, but feel free to experiment with the never versions, as everything should work accordingly.

To install it go to
```~/.steam/root/```
if there is a folder called ```compatibilitytools.d``` then go into this directory, if not just create it. When in the directory unzip the contents of the custom Proton instance in there and restart steam (or open it).

In Steam select The Elder Scrolls V: Skyrim SE and right cilck in the sidebar to open up the properties for said game. Navigate to GENERAL and check the box at the bottom ```Force to use a specific Steam Play compatibility tool```, then choose
our custom Proton, in this case ```Proton-5.0-GE-1```, but depending on which version you installed you of course need to adapt this choice.


Now you can tecnically also pplay Skyrim SE without mods from this point maybe even more stable than just with the sound fix, since this custom proton version should be better optimized however the next step is to install SKSE.
Head over to [SKSE's homepage](https://skse.silverlock.org/) and download the desired version, which should be at the moment ```Current SE build 2.0.17```, but this may change over time. Just make sure that you grab the SE edition!
unzip the file and drop the contents of skse64_2_00_17 into
 ```/<path to your SteamLibrary>/steamapps/common/Skyrim Special Edition/``` (where you should adapt ```<path to your SteamLibrary>``` accordingly).

Next we need to adapt the launcher to make the game actually start with SKSE. For this we simply rename ```SkyrimSELauncher.exe``` to ```SkyrimSELauncher.exe.old``` (just to keep a copy of the original in case something goes wrong)
and ```skse_loader.exe``` to ```SkyrimSELauncher.exe```.

Start the game via Steam and check if SKSE is working properly. When in the main menu of the game open the console (~ key, the key below the esc button dependin on keyboardlayout it could also be the § key)
and type ```getskseversion``` if a version number for SKSE is returned the installation was successfull. We can close Skyrim SE for now or enjoy the vanilla game.

This concludes the installation of SKSE.


## 4. Lutris, Vortex and Modding

Technically we can already install mods manually and enable them with the Bethesda launcher integrated in the game, but this can and will lead to bad load order and other problems also certain mods will simply not work
one such example is [SkyUI](https://www.nexusmods.com/skyrimspecialedition/mods/12604), though this could easily be fixed via the mod authors they simply choose not to but I digress, of course we will provide a fix for this situation.

This is where Lutris and Vortex come into play, first install [Lutris](https://lutris.net/downloads/) and start ther application. Next we need a launcher for Vortex, which can be downloaded [here](https://lutris.net/games/vortex-mod-manager/),
simply download and install it via Lutris. When install start Vortex via Lutris.
When installed go to games and search for Skyrim Special Edition, click on the ```+``` button and manually set the path to the game, it might look something like this, but you need to adjust it accordingly.
```
Z:\<path to SteamLibrary>\steamapps\common\Skyrim Special Edition
```
So far so good, now we can download mods from the [Nexus](https://www.nexusmods.com/skyrimspecialedition), but we always have to download them manually. When downloaded go to MODS in the Vortex mod manager and simply drop the archive to the drop file(s) section at the bottom
from there you can install your mods. Under Plugins you can see all the active plugins.

So here begins the fun, now you can install all your favourite mods in this way (including SkyUi, yes FNIS also works), but there is a small problem, we can not launch the game from the Vortex mod manager.
But worry not, as I also provide a very nice solution for this. Here comes the point where you could technically just install your mods and fire up Skyrim SE via steam and manually enable all of them inside the game via the MODS category and then Load Order.
As I mentioned before this yields a bad loading oreder and even worse, certain mods will fail to work (i.e. SkyUI).

Here comes the better method which I created:

First go to 
```
/<path to Lutris>/vortex-mod-manager/drive_c/users/alucard/Application Data/Vortex/skyrimse/profiles/5F9AFm9Oc/
```
there you will find a file called ```plugins.txt```.

Copy this file to your desktop and navigate to
```
/<path to SteamLibrary>/steamapps/compatdata/489830/pfx/drive_c/users/steamuser/Local Settings/Application Data/Skyrim Special Edition/
```
here you find a file called ```Plugins.txt```.

For troubleshooting reasons also make a copy of this file to your desktop.

So what does this mean? Well the first path is the mod load order set by vortex, which is well sorted and has enabled all our desired plugins as seen in the Vortex Mod Manager. The second path gives the load order (plugin list)
which is set by the integrated "mod manager" which comes with the game. If you would try to enable SkyUI in the game it would never appear on this list and hence never work (It will not work, because the SkyUi mod has not set a master file duiring creation, usually Skyrim is set as the master file to trick the integrated mod manager to load the mod).

Long story short, just delete the ```Plugins.txt``` from 
```
/<path to SteamLibrary>/steamapps/compatdata/489830/pfx/drive_c/users/steamuser/Local Settings/Application Data/Skyrim Special Edition/
```
rename ```plugins.txt``` from 
```
/<path to Lutris>/vortex-mod-manager/drive_c/users/alucard/Application Data/Vortex/skyrimse/profiles/5F9AFm9Oc/
```
to ```Plugins.txt``` and put it in 
```
/<path to SteamLibrary>/steamapps/compatdata/489830/pfx/drive_c/users/steamuser/Local Settings/Application Data/Skyrim Special Edition/
```

Now every time you change or add mods you need to replace these files, in order to trick Skyrim SE to use all your mods in the right order. Sometimes you need to restart Vortex for the ```plugins.txt``` file to update it. After doing so we may close Vortex.

Finally we are ready to start Skyrim SE via Steam, when in the main menu DO NOT and I repeat DO NOT click on the MODS category, this will reset our plugins file and nothing will work (you would need to repeat the above steps to copy the right plugins file again)
simply click new game or continue et voilà, everything should work just fine.


I hope the tutorial was helpful and not to complicated to follow, it actually took me quite a while to get SkyUI to work properly in Skyrim SE which is also the reason why I created this tutorial in the first place. Why do we care about SkyUI? Simply because of the MCM functionality.

Last I wish everyone happy modding!!!
